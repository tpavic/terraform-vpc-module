variable "repo_name" {
    description = "The name of the repository"
    type = string
}

variable "default_tags" {
    type    = map
    default = {}
}
