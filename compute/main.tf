

resource "aws_instance" "app_instance" {
    count = length(var.app_instances)
    ami = aws_ami_from_instance.my-ami.id
    #key_name = aws_key_pair.tpavic-key-pair.id
    instance_type = "t2.micro"
    subnet_id = element([for sub in var.priv_sub_id: sub ],count.index)
    vpc_security_group_ids = [var.pub_ssh_security_group]

    root_block_device {
      volume_size = 10 
      volume_type = "gp2"
  }
    tags = {
      Name = var.app_instances[count.index]
  }
}

resource "aws_instance" "bastion_instance" {
    count = length(var.bastion_instances)
    ami = aws_ami_from_instance.my-ami.id
    #key_name = aws_key_pair.tpavic-key-pair.id
    instance_type = "t2.micro"
    subnet_id = element([for sub in var.pub_sub_id: sub ],count.index)
    vpc_security_group_ids = [var.private_ssh_security_group]
    
    root_block_device {
      volume_size = 10 
      volume_type = "gp2"
  }
    tags = {
      Name = var.bastion_instances[count.index]
  }
}