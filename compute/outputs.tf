output "app_instance" {
    value = aws_instance.app_instance[*].id
}

output "bastion_instance" {
    value = aws_instance.bastion_instance[*].id
}

output "public_ip" {
  value       = aws_instance.bastion_instance[*].public_ip
}

output "private_dns" {
  value = aws_instance.app_instance[*].private_dns
}