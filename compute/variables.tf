variable "ami_name" {
    type = string
    description = "Name for custom AMI"
}

variable "ssh-pubkey" {
    type = string
    description = "SSH key used for login"
}

variable "app_instances" {
    type = list
    description = "List of app instance names"
}

variable "bastion_instances" {
    type = list
    description = "List of bastion instance names"
}

variable "priv_sub_id" {
    type = list
    description = "List of private subnets"
}

variable "pub_sub_id" {
    type = list
    description = "List public subnets"
}

variable "pub_ssh_security_group" {
    type = string
    description = "ID of the security group for public SSH access"
}

variable "private_ssh_security_group" {
    type = string
    description = "ID of the security group for private SSH access"
}