data "aws_ami" "amazon_linux_2" {
  owners = ["amazon"]
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-ebs"]
  }
}

resource "aws_key_pair" "tpavic_key_pair" {
  key_name   = "tpavic-key"
  public_key = file(var.ssh-pubkey)
}

resource "aws_instance" "my_ec2" {
    ami = data.aws_ami.amazon_linux_2.id
    key_name = aws_key_pair.tpavic_key_pair.id
    instance_type = "t2.micro"
    
    root_block_device {
      volume_size = 10 
      volume_type = "gp2"
  }
    tags = {
      Name = "Template"
  }
}


resource "aws_ami_from_instance" "my-ami" {
  name               = var.ami_name
  source_instance_id = aws_instance.my_ec2.id
}