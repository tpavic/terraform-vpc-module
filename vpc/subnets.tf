 resource "aws_subnet" "public_subnet" {
  for_each = var.public_subnet
 
  availability_zone = each.value["az"]
  cidr_block = each.value["cidr"]
  vpc_id     = aws_vpc.main.id
  map_public_ip_on_launch = true

  tags = {
    Name = "${each.value["name"]}"
  }
}
 resource "aws_subnet" "private_subnet" {
  for_each = var.private_subnet
 
  availability_zone = each.value["az"]
  cidr_block = each.value["cidr"]
  vpc_id     = aws_vpc.main.id

  tags = {
    Name = "${each.value["name"]}"
  }
}
