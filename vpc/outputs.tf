output "vpc_id" {
  value       = aws_vpc.main.id
  description = "The ID of the VPC"
}

output "public_subnets" {
 description = "List of public subnets"
  value = aws_subnet.public_subnet
}

output "private_subnets" {
  description = "List of private subnets"
  value = aws_subnet.private_subnet
}

