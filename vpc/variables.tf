variable "vpc_name" {
  description = "Name, tag for the vpc"
  type        = string
}

variable "my_cidr_block" {
  description = "CIDR for the VPC"
  type        = string
}

variable "public_subnet" {
   description = "Specification of public subnet"
   type = map
}

variable "private_subnet" {
   description = "Specification of private subnet"
   type = map
}