resource "aws_vpc" "main" {
  cidr_block = "${var.my_cidr_block}"
  enable_dns_hostnames = true

  tags = {
    Name = "${var.vpc_name}"
  }
}