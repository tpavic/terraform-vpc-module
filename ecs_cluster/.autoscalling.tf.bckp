data "aws_ami" "ecs_ami" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-ecs-hvm-*-x86_64-ebs"]
  }
}

resource "aws_launch_configuration" "ecs_launch_config" {
    image_id             = "${data.aws_ami.ecs_ami.id}"
    iam_instance_profile = aws_iam_instance_profile.ecs_agent.name
    key_name             = var.ssh_pubkey
    security_groups      = [var.private_security_group.id]
    user_data            = <<-EOF
                         #!/bin/bash
                         echo ECS_CLUSTER=Koffee-Luv-Cluster >> /etc/ecs/ecs.config;
                         echo ECS_BACKEND_HOST= >> /etc/ecs/ecs.config;
                         EOF
    instance_type        = "t2.micro"
}

resource "aws_autoscaling_group" "failure_analysis_ecs_asg" {
    name                      = "asg"
    vpc_zone_identifier       = [var.priv_sub_id]
    launch_configuration      = aws_launch_configuration.ecs_launch_config.name
    desired_capacity          = 2
    min_size                  = 1
    max_size                  = 3
    health_check_grace_period = 300
    health_check_type         = "EC2"
}