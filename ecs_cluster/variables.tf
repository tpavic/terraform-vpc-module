variable "app_name" {
  type        = string
  description = "Application Name"
}

variable "app_environment" {
  type        = string
  description = "Application Environment"
}

variable "vpc_id" {
  type        = string
} 

variable "aws_region" {
  type        = string
  description = "AWS Region"
} 

variable "ecr_repository_url" {
    type = string
}

variable "ecr_image_tag" {
    type    = string
    default = "latest"
}

variable "td-service_port" {
    type = number
}

variable "td-cpu" {
    type = string
    default = "256"
}

variable "td-memory" {
    type = string
    default = "512"
}

variable "desired_task_count" {
    type = number
    default = "1"
}

#variable "private_security_group" {
#    type = string
#}

variable "private_subnet_id" {
    type = list
}

variable "public_subnet_id" {
    type = list
}

variable "domain_name" {
    type = string
}

#variable "ssh_pubkey" {
#    type = string
#}
