variable "app_instances" {
    description = "List of app instances"
    type = list
}

variable "bastion_instances" {
    description = "List of bastion instances"
    type = list
}

variable "vpc_id" {
    description = "ID for the selected VPC"
    type = string
}

variable "cidr_block" {
    description = "All public access"
    type = string
}


