resource "aws_security_group" "public_traffic" {
  name        = "allow_public_ssh"
  description = "Allow public SSH inbound traffic"
  vpc_id      = var.vpc_id

  ingress {
    description      = "SSH"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = [var.cidr_block]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = [var.cidr_block]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_public_ssh"
  }
}

resource "aws_security_group" "private_traffic" {
  name        = "allow_private_ssh"
  description = "Allow private SSH inbound traffic"
  vpc_id      = var.vpc_id

   tags = {
    Name = "allow_private_ssh"
  }
}

resource "aws_security_group_rule" "allow_ssh" {
  type              = "ingress"
  to_port           = 22
  from_port         = 22
  protocol          = "tcp"
  source_security_group_id = aws_security_group.public_traffic.id
  security_group_id = aws_security_group.private_traffic.id
}

resource "aws_security_group_rule" "allow_443" {
  type              = "ingress"
  to_port           = 443
  from_port         = 443
  protocol          = "tcp"
  cidr_blocks      = [var.cidr_block]
  security_group_id = aws_security_group.private_traffic.id
}

resource "aws_security_group_rule" "allow_all" {
  type              = "egress"
  to_port           = 0
  from_port         = 0
  protocol          = "tcp"
  cidr_blocks      = [var.cidr_block]
  security_group_id = aws_security_group.private_traffic.id
}