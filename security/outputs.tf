output "public_security_group" {
    value = aws_security_group.public_traffic.id
}

output "private_security_group" {
    value = aws_security_group.private_traffic.id
}