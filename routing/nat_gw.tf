locals {
  eip = [ for ip in aws_eip.nat : ip.id ]
}

locals {
  nat_gw = [ for gw in aws_nat_gateway.ngw : gw.id ]
}
resource "aws_eip" "nat" {
  for_each = var.public_subnet
  vpc = true

 # lifecycle {
 #    prevent_destroy = true
  #}
}


resource "aws_nat_gateway" "ngw" {
  count          = length(var.public_subnet)
  subnet_id      = local.public_subnet_ids[count.index]
  allocation_id =  local.eip[count.index]

  tags = {
    Name = var.nat_gateway[count.index]
  }
}