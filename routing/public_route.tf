locals {
  public_subnet_ids = [for subnet in var.public_subnet: lookup("${subnet}", "id")]
}

locals {
  public_subnets = [ for subnet in var.public_subnet : subnet.id ]
}

resource "aws_internet_gateway" "igw" {    
    vpc_id =  var.vpc_id

    tags = {
    Name   = "${var.igw_name}"
    }
}


resource "aws_route_table" "public" {
  vpc_id = var.vpc_id
 
  route {
    cidr_block = var.cidr_block
    gateway_id = aws_internet_gateway.igw.id
  }
 
  tags = {
    Name   = var.public_rt
  }
}

resource "aws_route_table_association" "public" {
  for_each       = {for i,v in local.public_subnet_ids: i => v}
  subnet_id      = each.value
  route_table_id = aws_route_table.public.id
}

