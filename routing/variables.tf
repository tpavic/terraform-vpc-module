variable "igw_name" {
    description = "Name for the IGW"
    type = string
}

variable "cidr_block" {
    description = "CIDR for incoming traffic"
    type = string
}

variable "public_rt" {
    description = "Name for public route"
    type = string
}

variable "private_rt" {
    description = "Name for private route"
    type = map
}

variable "vpc_id" {
    description = "ID for the selected VPC"
    type = string
}

variable "public_subnet" {
    description = "List of public subnet id's"
}

variable "private_subnet" {
    description = "List of private subnet id's"
}

variable "nat_gateway" {
    description = "List of NAT Gateways"
    type = list
}
