output "public_subnet_ids" {
    description = "Public subnet IDs"
    value=local.public_subnets
}

output "private_subnet_ids" {
    description = "Public subnet IDs"
    value=local.private_subnets
}

output "cidr_block" {
    description = "All public access"
    value = var.cidr_block
}

output "eip" {
    value = aws_nat_gateway.ngw[*].public_ip
}