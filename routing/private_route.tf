locals {
  private_rt = [ for route in aws_route_table.private : route.id ]
}

locals {
  private_subnets = [ for subnet in var.private_subnet : subnet.id ]
}

resource "aws_route_table" "private" {
  for_each = var.private_rt
  vpc_id = var.vpc_id
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id          = aws_nat_gateway.ngw[each.key].id
  }
  tags = {
    Name   = each.value
  }
}

 
resource "aws_route_table_association" "private_subnet" {
  count          = length(var.private_subnet)
  subnet_id      = local.private_subnets[count.index]
  route_table_id = element(local.private_rt, count.index)
}